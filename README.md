Funcionalidade 1 

Mensagem de texto. 

Sendo um usuário do aplicativo WhatsApp é possível 
trocar mensagens de textos com um ou mais usuários que possuem 
o aplicativo instalado.

Cenário  

Enviar uma mensagem de texto para outro usuário ou para um grupo especifico

*Dado que estou na lista de contatos escolho um contato, 
abrirá a janela de conversa, escrevo o texto que desejo e clico
na tecla enter

*Dado que estou com o aplicativo aberto e com a lista de conversas disponíveis
abro um grupo, clico em "digite aqui", escrevo um texto que desejo e clico na 
tecla enter


Funcionalidade 2 

Compartilhar Músicas

Sendo um usuário do aplicativo WhatsApp é possível compartilhar músicas
com um ou mais usuários que possuem o aplicativo instalado


Cenário

compartilhar música com um usuário ou um grupo especifico

*Dado que estou com uma conversa aberta com outro usuário, 
clico no ícone clips e seleciono a opção áudio, em seguida, escolho a ação
"escolher a faixa de" , seleciono a musica desejada e clico em Ok. 

*Dado que estou com uma conversa aberto de um grupo,clico no ícone clips e seleciono
a opção áudio, em seguida, escolho a ação " escolher a faixa de", seleciono a música desejada
e clico em ok